﻿using System;
using RabbitMQ.Client;
using System.Text;
using System.Threading;

namespace Send
{
    class Send
    {
        public static void Main(string[] args)
        {

            // ---------------------- เปิด Connection เพื่อเชื่อมต่อไปยัง RabbitMQ Server ------------------------------------------
            var factory = new ConnectionFactory()
            {
                Uri = new Uri("amqp://localhost:5672")
            };

            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            // -------------------------------------------------------------------------------------------------------------


            // -------------------- ประกาศ Queue, Exchange, ผูก Queue เข้ากับ Exchange ก่อน publish message --------------------
            QueueDeclare(channel);
            ExchangeDeclare(channel);
            BindQueueExchange(channel);
            // ------------------------------------------------------------------------------------------------------------


            // --------------------- ประกาศ คุณสมบัติ เพิ่มเติมให้กับ การ publish message ไปยัง RabbitMQ Server ---------------------
            var properties = channel.CreateBasicProperties();
            properties.Persistent = true;
            // -----------------------------------------------------------------------------------------------------------



            for (int i = 0; true; i++)
            {
                var msg = string.Format("Message number {0}", i+1);  
                // ------------------------------ เปลี่ยน string message  เป็น Byte Array ก่อนส่ง ------------------------------
                var body = Encoding.UTF8.GetBytes(msg);
                // -------------------------------------------------------------------------------------------------------



                // --------------------------------- publish message ไปยัง RabbitMQ Server ---------------------------------
                channel.BasicPublish(
                    exchange: "",
                    routingKey: "msg.queue",
                    basicProperties: properties,
                    body: body
                );
                // -------------------------------------------------------------------------------------------------------

            }

        }




        private static void QueueDeclare(IModel channel)
        {
            // สร้าง queue ขึ้นมาใน RabbitMQ Server เมื่อรันโค๊ด
            // หากมี queue อยู่แล้ว จะไม่สร้างซ้ำ

            channel.QueueDeclare(
                queue: "msg.queue",
                durable: false,
                exclusive: false,
                autoDelete: true,
                arguments: null
            );
        }


        private static void ExchangeDeclare(IModel channel)
        {
            // สร้าง exchange ขึ้นมาใน RabbitMQ Server เมื่อรันโค๊ด
            // หากมี exchange อยู่แล้ว จะไม่สร้างซ้ำ
            channel.ExchangeDeclare(
                exchange: "msg.exchange",
                type: ExchangeType.Direct
            );
        }

        private static void BindQueueExchange(IModel channel)
        {
            // ทำการเชื่อมต่อ queue กับ exchange ขึ้นมาใน RabbitMQ Server เมื่อรันโค๊ด
            // หากมีการเชื่อมต่อ queue กับ exchange อยู่แล้ว จะไม่ทำซ้ำ
            channel.QueueBind(
                queue: "msg.queue",
                exchange: "msg.exchange",
                routingKey: "msg.send"
            );
        }

    }
}
