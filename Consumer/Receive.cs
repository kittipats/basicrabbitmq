﻿using System;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Threading.Tasks;

namespace Receive
{
    class Receive
    {
        public static void Main(string[] args)
        {

            // ---------------------- เปิด Connection เพื่อเชื่อมต่อไปยัง RabbitMQ Server ------------------------------------------
            var factory = new ConnectionFactory()
            {
                Uri = new Uri("amqp://localhost:5672")
            };

            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            // -------------------------------------------------------------------------------------------------------------



            // -------------------- ประกาศ Queue, Exchange, ผูก Queue เข้ากับ Exchange ก่อน publish message --------------------
            QueueDeclare(channel);
            ExchangeDeclare(channel);
            BindQueueExchange(channel);
            // ------------------------------------------------------------------------------------------------------------






            // --------------------------- กำหนด prefetch ให้แก่ Consumer ที่รับ message แบบ Push API ---------------------------

            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            // ------------------------------------------------------------------------------------------------------------


            // ----------------- สร้าง Consumer และ logic สำหรับ process เมื่อได้รับ message มาจาก RabbitMQ Server -----------------
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);


                // ---------------- ส่ง acknowledge กลับไปยัง RabbitMQ Server เพื่อบอกว่า ได้รับ message มาเรียบร้อยแล้ว ----------------
                // --------------------------- ใช้เมื่อ channel.BasicConsume(autoAck: [เท่ากับ true]) ---------------------------

                channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                // ------------------------------------------------------------------------------------------------------

            };



            // ---------------------------------- เริ่ม Consume Message จาก RabbitMQ Server ----------------------------------
            channel.BasicConsume(
                queue: "msg.queue",
                autoAck: false,
                consumer: consumer
            );
            // ----------------------------------------------------------------------------------------------------------


        }




        private static void QueueDeclare(IModel channel)
        {
            // สร้าง queue ขึ้นมาใน RabbitMQ Server เมื่อรันโค๊ด
            // หากมี queue อยู่แล้ว จะไม่สร้างซ้ำ

            channel.QueueDeclare(
                queue: "msg.queue",
                durable: false,
                exclusive: false,
                autoDelete: true,
                arguments: null
            );
        }


        private static void ExchangeDeclare(IModel channel)
        {
            // สร้าง exchange ขึ้นมาใน RabbitMQ Server เมื่อรันโค๊ด
            // หากมี exchange อยู่แล้ว จะไม่สร้างซ้ำ
            channel.ExchangeDeclare(
                exchange: "msg.exchange",
                type: ExchangeType.Direct
            );
        }

        private static void BindQueueExchange(IModel channel)
        {
            // ทำการเชื่อมต่อ queue กับ exchange ขึ้นมาใน RabbitMQ Server เมื่อรันโค๊ด
            // หากมีการเชื่อมต่อ queue กับ exchange อยู่แล้ว จะไม่ทำซ้ำ
            channel.QueueBind(
                queue: "msg.queue",
                exchange: "msg.exchange",
                routingKey: "msg.send"
            );
        }






        public static async void delay(int time, Action execute)
        {
            await Task.Delay(time);
            execute();
        }
    }
}
